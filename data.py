from faker import Faker


fake = Faker()

# Define the API base URL
url = "http://localhost:8000"

# Generate random user data
new_user = {
    "user": {
        "first_name": fake.first_name(),
        "last_name": fake.last_name(),
        "email": fake.email(),
        "password": fake.password(length=8)  
    },
    "address": {
        "address_1": fake.street_address(),
        "address_2": fake.secondary_address(),
        "city": fake.city(),
        "state": fake.state(),
        "zip": fake.zipcode(),
        "country": fake.country()
    }
}

# Define the country
country = "Vietnam"